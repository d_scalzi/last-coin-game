# Last Coin Game #

![r8osm.jpg](https://bitbucket.org/repo/KqoR6r/images/3087573974-r8osm.jpg)

### [Android Version](https://bitbucket.org/AventiumSoftworks/last-coin-game-android/downloads) ###

### What is the game? ###

* Select an amount of coins to play with.
* Choose your difficulty settings.
* Take one or two coins per round and try to force your opponent (the ai) to take the last coin.
* This version uses swing even though I hate swing. I am ashamed for not using JavaFX initially.

### How do I get set up? ###

* Download and run the jar.
* Must meet system requirements posted below.

### System Requirements for Jar ###

* Java 8 (you can use Java 7 if you really want to)

## Download ##

Java 7 version not always up to date

* [Java 8 (Recommended)](https://drive.google.com/file/d/0B87LBoj2FKG4d0w3c0lWUTVtbVk/view) Version 1.1.5

* [Java 7 (Will eventually be unsupported)](https://drive.google.com/file/d/0B87LBoj2FKG4VGVtQ1BvMUNTMTQ/view) Version 1.1.2

### Contact ###

* Repo owner or admin (Daniel Scalzi)